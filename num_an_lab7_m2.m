N = 10;
eps = 0.00001;

A=(rand(N))*10;
A=(A+A')/2;
B=inv(A);

x=zeros(N,100);
l = zeros(1,100);
err = zeros(1,100);

err(1)=realmax;
x(:,1) = ones(N,1);
i=1;

while err(i)>eps
    i=i+1;
    x(:,i-1) = x(:,i-1)/norm(x(:,i-1));
    x(:,i) = B*x(:,i-1);
    l(i) = x(1,i-1)/x(1,i);
    err(i)=norm(A*x(:,i)-l(i)*x(:,i));
    if i>100000 break; end
end
disp(eig(A));
disp(l(i));

scatter(2:1:i,err(2:i),30,"filled");
set(gca,'yscale','log');
xlabel("iteration");
ylabel('ln\epsilon','Rotation',0);
fontsize(gca,20,"points")